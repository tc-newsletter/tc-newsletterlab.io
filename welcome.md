---
layout: page
title: Dashboard
permalink: /
---

# 2018-newsletter-template-design-and-code

Frequently used links
- [Topcoder newsletter coummunity slack channel](https://topcodercommunity.slack.com/messages/G9JQNDU5N/)
- [**Guidelines for creating newsletter** (Google docs)](https://docs.google.com/document/d/1OKpio6xC1WL2lAHByX7NCNe6ZDpzOqQha5DoP6NXA_M/edit)
- [2018-newsletter-template-design-and-code (Google docs)](https://docs.google.com/document/d/1MaxbXabgRezdBYQYPohaBn9AG8Iw4oOcBOI1de3xu5s/edit#heading=h.6n586k251s1g)
- [**Newsletter banner designs** (Googel drive)](https://drive.google.com/drive/folders/1KzgXzIDZ-TS5JwKWtlhi8Um0yWWX0Gy9)
- [Newbie newsletters](https://docs.google.com/document/d/160uTHN5aQh-kfzEsMo6XFrnxel23rwa1cVmwMd8LnHE/edit) 

- [Markdown format keywords (Gitlab)](https://gitlab.com/help/user/markdown)
- [Request new enhacement, feature or template](https://gitlab.com/tc-newsletter/newsletter-templates/issues/new)
- [Report bugs 🐞](https://gitlab.com/tc-newsletter/newsletter-templates/issues/new)



